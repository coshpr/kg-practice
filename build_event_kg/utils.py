import os
import xml.etree.ElementTree as ET


def get_dir_all_file():
    corpus_path = "./corpus/CEC"
    fileDir = os.sep.join([".", "corpus", "CEC"])  # 以分隔符连接路径名
    files_List = []
    for root, dirs, files in os.walk(fileDir):
        # print("root:", root)
        # dirs
        # for dir in dirs:
        #     print(os.path.join(root, dir))
        for file in files:
            files_List.append(os.path.join(root, file))
    return files_List


def analysis_xml_tree(file_path):
    print(file_path)
    tree = ET.parse(file_path)
    root = tree.getroot()
    print(root)

    # 事件关系
    for child in root.findall("eRelation"):
        print(child.tag, child.attrib)

    for p in root.findall("Content/Paragraph"):
        for s in p.findall("Sentence"):
            for e in s.findall("Event"):
                print(e.tag, e.attrib)
                for item in e:
                    print(item.tag, item.attrib, item.text)
        # id = item.find('eid')
        # print(id)


if __name__ == '__main__':
    lists = get_dir_all_file()
    analysis_xml_tree(lists[0])
